function moveButtonOnControlPanel() {
  var academicbt = $('.control-panel-academic-integration');
  if (academicbt.length > 0) {
    academicbt.insertAfter('.control-panel-edit-profile');
  }
}

function removeButtonForNonStudents() {
  if (!$('html').hasClass('member-of-estudantes')) {
    var academicbt = $('.control-panel-academic-integration');
    if (academicbt.length > 0) {
      academicbt.remove();
    }
  }
}

jQuery( document ).ready(function( $ ) {
  removeButtonForNonStudents();
  moveButtonOnControlPanel();
});
