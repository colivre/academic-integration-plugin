#!/bin/env ruby

require File.dirname(__FILE__) + '/../../../../test/test_helper'

class AcademicIntegrationPluginTest < ActiveSupport::TestCase

  def setup
    @plugin = AcademicIntegrationPlugin.new
  end

  attr_reader :plugin

  should 'define after filter for account controller' do
    assert_equal :after_filter, plugin.account_controller_filters.first[:type]
  end

end
