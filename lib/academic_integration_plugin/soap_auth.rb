require 'soap/rpc/driver'

class AcademicIntegrationPlugin::SoapAuth < Noosfero::Plugin

  attr_accessor :login, :password

  def hash_for_authentication(login, passwd)
    server = AcademicIntegrationPlugin.new.webservice_url
    driver = SOAP::RPC::Driver.new(server)
    driver.add_method('wsLogin', 'username', 'passwd')
    return driver.wsLogin(login, passwd)
  end

end
