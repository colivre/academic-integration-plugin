class AcademicIntegrationPlugin < Noosfero::Plugin
  include Noosfero::Plugin::HotSpot

  CONFIG = YAML.load_file(AcademicIntegrationPlugin.root_path.join('config.yml'))

  def self.plugin_name
    "Academic Integration"
  end

  def self.plugin_description
    _("A plugin to integrate with Sagu/Solis GE.")
  end

  def control_panel_button_name
    CONFIG[:control_panel_button_name] || _('Academic link')
  end

  def control_panel_buttons
    {:title => control_panel_button_name, :icon => 'academic-integration', :url => { controller: 'academic_integration_plugin_myprofile', action: 'index', profile: context.profile.identifier }}
  end

  def webservice_url
    CONFIG[:webservice_url] || ''
  end

  def academic_app_url
    CONFIG[:academic_app_url] || ''
  end

  def cookie_name
    CONFIG[:cookie_name] || 'authenticationHash'
  end

  def stylesheet?
    true
  end

  def account_controller_filters
    cookie_key = cookie_name
    set_webservice_cookie_block = proc do
      if request.post? && logged_in?
        hash = AcademicIntegrationPlugin::SoapAuth.new.hash_for_authentication(params[:user][:login], params[:user][:password])
        cookies[cookie_key] = {
          value: hash,
          expires: 5.minutes.from_now,
          domain: :all
        }
      end
    end

    [{:type => :after_filter,
      :method_name => :set_webservice_cookie,
      :options => {:only => 'login'},
      :block => set_webservice_cookie_block }
    ]
  end

  def js_files
    'control-panel.js'
  end

end
