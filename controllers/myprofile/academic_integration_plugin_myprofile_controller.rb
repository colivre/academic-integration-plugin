require "net/http"

class AcademicIntegrationPluginMyprofileController < MyProfileController

  def index
    cookie_key = AcademicIntegrationPlugin.new.cookie_name
    if cookies[cookie_key].blank?
      redirect_to action: 'confirm_password'
    else
      redirect_to_app(cookie_key)
    end
  end

  def confirm_password
    if request.post?
      cookie_key = AcademicIntegrationPlugin.new.cookie_name
      if User.authenticate(user.user.login, params[:password])
        hash = AcademicIntegrationPlugin::SoapAuth.new.hash_for_authentication(user.user.login, params[:password])
        cookies[cookie_key] = {
          value: hash,
          expires: 28.minutes.from_now,
          domain: :all
        }
      redirect_to_app(cookie_key)
      else
        session[:notice] = _('Password incorrect, please try again')
      end
    end
  end

  def redirect_to_app(cookie_key)
    app_url = AcademicIntegrationPlugin.new.academic_app_url
    begin
      uri = URI.parse(app_url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.read_timeout = 10 # seconds
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      redirect_to app_url + "&#{cookie_key}=#{cookies[cookie_key]}"
    rescue Timeout::Error
      session[:notice] = _('The Academic Aplication is not available. Please, try again later')
      redirect_to user.url
    end
  end
end
